import Vue from 'vue'
import App from './App.vue'

/*
*
* COMPONENTS
*
*/
const Banner = () => import('@/components/Banner/Banner.vue');
const Rating = () => import('@/components/Rating/Rating.vue');

/*
*
* GLOBAL COMPONENT REGISTRATION
*
*/
Vue.component('Banner', Banner);
Vue.component('Rating', Rating);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
