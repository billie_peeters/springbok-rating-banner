export default {
    name: 'Banner',

    props: [ 'bannerType', 'percentage', 'rating' ],

    watch: {
        percentage: function () {
            switch (true) {
                case (this.percentage > 100):
                    this.percentageValue = 100;
                    break;
                case (this.percentage < 0):
                    this.percentageValue = 0;
                    break;
                case (this.percentage < 100 && this.percentage > 0):
                    this.percentageValue = this.percentage;
                    break;
                default:
                    this.percentageValue = 0;
                    break;
            }
        },
    },

    data() {
        return {
            percentageValue: 0,
        }
    },
}