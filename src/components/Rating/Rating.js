export default {
    name: 'Rating',

    props: [ 'rating' ],

    watch: {
        rating: function () {

            switch (true) {
                case (this.rating > 5):
                    this.ratingValue = 5;
                    break;
                case (this.rating < 0):
                    this.ratingValue = 0;
                    break;
                case (this.rating <= 5 && this.rating > 0):
                    this.ratingValue = this.rating;
                    break;
                default:
                    this.ratingValue = 0;
                    break;
            }
        },
    },

    data() {
        return {
            ratingValue: 0,
            startAmount: 5,
        }
    },
}